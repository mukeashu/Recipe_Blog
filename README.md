# My Recipe App



## Getting started

The first step to launch the project is to make sure your docker engine is running.

On Linux:

Run the setup.sh script localized in /Backend/app directory. 
It maybe necessary to grant the file execute permission.

On Windows:

It is easier to just run the following commands in the same directory.

docker-compose build
docker-compose up -d

If you do not have npm installed, make sure to do it now with the command:

pip3 install npm

If this is your first time running the project run navigate to the Web directory and run:

npm install


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mazlumatabey/my_receip_app.git
git branch -M main
git push -uf origin main
```


