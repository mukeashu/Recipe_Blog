from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS
import sqlalchemy
import time

UPLOAD_FOLDER = '/app'

app = Flask(__name__)
CORS(app)
app.config["SQLALCHEMY_DATABASE_URI"] = "mariadb+mariadbconnector://root:root@db:3306/MyReceipApp"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
db = SQLAlchemy(app)

from .user import User
from .article import Article

try_count = 0
while True:
    try:
        try_count = try_count + 1
        db.create_all()
        db.session.commit()
        break
    except sqlalchemy.exc.OperationalError as e:
        print(e)
        print("Error while connecting to db. Trying again in 1s")
        time.sleep(1)
    if try_count > 20:
        raise Exception("Failed to connect to db. Number of try {}", try_count)

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = '*'
    return response

@app.route("/")
def root():
    return "OK", 200