from crypt import methods
from operator import methodcaller
import os
import hashlib
import math
from flask import request
import json
from sqlalchemy_utils import aggregated
from . import db, app
from .user import User


class Article(db.Model):
    __tablename__ = "articles"
    article_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256), nullable=False)
    text = db.Column(db.Text, nullable=False)
    image_url = db.Column(db.String(128), unique=True, nullable=True)
    likes_nb = db.Column(db.Integer)
    author_id = db.Column(db.Integer)

    @classmethod
    def from_db(cls, title):
        return Article.query.filter_by(title=title).first()

    @classmethod
    def from_db_id(cls, article_id):
        return Article.query.filter_by(article_id=article_id).first()

    @classmethod
    def exist(cls, title):
        return Article.query.filter_by(title=title).first() is not None

    @classmethod
    def get_articles(cls, page):
        if page == '?':
            page = 0
#        articles = Article.query.all()[page * 10: (page + 1) * 10 - 1:1]
        articles = Article.query.all()
        return articles

    @classmethod
    def get_my_articles(cls, page, id):
        if page == '?':
            page = 0
#        articles = Article.query.filter_by(author_id=id)[page * 10: (page + 1) * 10 - 1:1]
        articles = Article.query.filter_by(author_id=id)
        return articles

    @classmethod
    def rate(cls, article_id):
        to_modify = db.session.query(Article).filter(Article.article_id == article_id)
        to_modify.likes_nb = to_modify.likes_nb + 1
        return article

    @classmethod
    def unrate(cls, article_id):
        to_modify = db.session.query(Article).filter(Article.article_id == article_id)
        to_modify.likes_nb = to_modify.likes_nb + 1
        return article

    @classmethod
    def create(cls, title, text, author_id):
        article = Article(title=title, text=text, image_url=None, likes_nb=0, author_id=author_id)
        db.session.add(article)
        return article

    @classmethod
    def update(cls, title, new_title, new_text):
        to_modify = db.session.query(Article).filter(Article.title == title)
        to_modify.title = new_title
        to_modify.text = new_text

@app.route("/MyArticle/<id>", methods=["GET"])
@app.route("/MyArticle/<id>/<page>", methods=["GET"])
def myArticle(id, page='?'):
        print("[GET] /MyArticle")
        articles = Article.get_my_articles(page, id)
        tmp = []
        for article in articles:
            tmp.append({"id": article.article_id, "title": article.title, "text": article.text, "like_nb": article.likes_nb, "author": id});
        return json.dumps(tmp), 200

@app.route("/DeleteArticle", methods=["POST"])
def delete_article():
    print("[POST] /DeleteArticle")
    try:
        article_id = request.json.get("article")
        author_id = request.json.get("author")
    except Exception as e:
        return "Permission denied", 415
    article = Article.query.filter_by(article_id=article_id).first()
    print(int(author_id),int(article.author_id))
    if (int(author_id) != int(article.author_id)):
        return "You did not created this article", 416
    db.session.delete(article)
    db.session.commit()
    return "Article perfectly removed", 200

@app.route("/Article", methods=["POST", "GET"])
@app.route("/Article/<page>", methods=["POST", "GET"])
def article(page='?'):
    if (request.method == "GET"):
        print("[GET] /Article")
        articles = Article.get_articles(page)
        tmp = []
        for article in articles:
            tmpUser = User.from_db_id(article.author_id).username
            tmp.append({"title": article.title, "text": article.text, "like_nb": article.likes_nb, "author": tmpUser});
        return json.dumps(tmp), 200
    if (request.method == "POST"):
        print("[POST] /Article")
        try:
            title = request.json.get("title")
            text = request.json.get("text")
            author_id = request.json.get("author_id")
        except Exception as e:
            return {"status": "Invalid request."}, 401
        article = Article.create(title, text, author_id)
        db.session.commit()
        print("Article updated")
        return {"status": "Article created"}, 200