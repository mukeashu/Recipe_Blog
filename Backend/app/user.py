from cmath import exp
from crypt import methods
from operator import methodcaller
import os
import hashlib
import math
from flask import request
import json
from passlib.hash import sha256_crypt

from . import db, app


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)

    @classmethod
    def from_db(cls, email):
        return User.query.filter_by(email=email).first()

    @classmethod
    def from_db_id(cls, id):
        return User.query.filter_by(id=id).first()

    @classmethod
    def exist(cls, email):
        return User.query.filter_by(email=email).first() is not None

    @classmethod
    def get_users(cls):
        return User.query.all()

    @classmethod
    def create(cls, email, password, username):
        hashedPassword = sha256_crypt.encrypt(str(password))
        user = User(email=email, username=username, password=hashedPassword)
        db.session.add(user)
        return user

    @classmethod
    def update(cls, id, password, username):
        user = User.from_db_id(id)
        if (password != ""):
            hashedPassword = sha256_crypt.encrypt(str(password))
            user.password = hashedPassword
        user.username = username
        return user


@app.route("/login", methods=["POST"])
def login():
    print("[GET] /login")
    try:
        email = request.json.get("email")
        password = request.json.get("password")

    except Exception as e:
        return {"status": "Auth required."}, 471

    if not User.exist(email):
        return {"status": "No account exist with this email."}, 472

    user = User.from_db(email)

    if sha256_crypt.verify(password, user.password):
        return {
            "status": "Success",
            "user": {
                "email": user.email,
                "id": user.id,
                "username": user.username,
            }
        }, 200
    else :
        return {"status": "Wrong Password"}, 473

@app.route("/update", methods=["POST"])
def update():
    print("[POST] /update")
    try:
        username = request.json.get("username")
        password = request.json.get("password")
        id = request.json.get("id")
    except Exception as e:
        return {"status" "Something went wrong"}
    user = User.update(id, password, username)
    db.session.commit()
    print("user updated")
    return user.username, 200    


@app.route("/user", methods=["POST", "GET"])
@app.route("/user/<user_id>", methods=["POST", "GET"])
def user(user_id='?'):
    if request.method == "GET":
        print("[GET] /user")
        users = User.get_users()
        tmp = []
        for user in users:
            tmp.append({"email": user.email});
        return json.dumps(tmp), 200
    if request.method == "POST":
        print("[POST] /user")
        try:
            email = request.json.get("email")
            password = request.json.get("password")
            username = request.json.get("username")
        except Exception as e:
            return {"status": "Invalid request."}, 401

        user = User.create(email, password, username)
        db.session.commit()
        print("user created")
        return user.email, 200
