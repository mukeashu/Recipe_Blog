My Recipe App
Getting started

1. The first step to launch the project is to make sure your docker engine is running.

    On Linux:

        Run the setup.sh script localized in /Backend/app directory. It may be necessary to grant the file execute permission.

    On Windows:

        It is easier to just run the following commands in the /Backend directory.

            docker-compose build
            docker-compose up -d

2. If you do not have npm installed, make sure to do it now with the command:

    pip3 install npm

3. If this is your first time running the project run navigate to the Web directory and run:

    npm install

4. Otherwise navigate to the Web directory and run the command:

    npm start

The app should open in your browser automatically. Otherwise you can find it at localhost:8080np