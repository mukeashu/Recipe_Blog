import React, { useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import '../App.css';

function Login() {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [passwordError, setpasswordError] = useState("");
  const [emailError, setemailError] = useState("");

  const handleValidation = (event) => {
    let formIsValid = true;

    if (!email.match(/^\w.+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailError("Email Not Valid");
      return false;
    } else {
      setemailError("");
      formIsValid = true;
    }

    if (!password.match(/^[a-zA-Z0-9]{8,22}$/)) {
      formIsValid = false;
      setpasswordError(
        "Length must be between 8 and 22 Chracters"
      );
      return false;
    } else {
      setpasswordError("");
      formIsValid = true;
    }


    axios.post("http://localhost:5000/login", {
      email: email,
      password: password,
    })
      .then((e) => {
        localStorage.setItem("userId", e.data.user.id)
        localStorage.setItem("userEmail", e.data.user.email)
        localStorage.setItem("username", e.data.user.username)
        window.location.replace("http://localhost:8080/")
    })
      .catch((error) => {
      alert("An error occured")
      console.log(error);
    });


    return formIsValid;
  };

  const loginSubmit = (e) => {
    e.preventDefault();
    handleValidation();
    console.log(password)
    console.log(email)
  };

  return (
    <div className="my-forms">
      <div className="container" >
        <div className="row d-flex justify-content-center" >
          <div className="col-md-4" style={{ borderRadius: "10px", backgroundColor: "#f1f7fe", padding: "30px" }}>
          <h1 className="page-header">Login</h1>
            <form id="loginform" onSubmit={loginSubmit}>
              <div className="form-group">
                <label>Email address</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text">
                  {emailError}
                </small>
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text">
                  {passwordError}
                </small>
              </div>
                <button type="submit" className="btn btn-primary">
                    Login
                </button>
                <div>                              
                    <a href="/register">No account ?</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Login;

