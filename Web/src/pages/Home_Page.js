import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import NonConnected from "../components/notConnected";
import axios from "axios";
import {Card, Col, Container, ListGroup} from "react-bootstrap";
import Articles from "../components/Articles";

function HomePage() {

  return (
    <div className="HomePage">
        <Articles></Articles>
    </div>
  );
}
export default HomePage;