import React, {Component, useState} from 'react';
import {Button, Container, Form, FormControl, Nav, Navbar} from "react-bootstrap";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Login from "../pages/login";
import Register from "../pages/register";
import Profile from '../pages/Profile';
import AboutUs from '../pages/aboutUs';
import AddPost from '../pages/Add_Post';
import HomePage from '../pages/Home_Page';

function Header() {

    const [connected, setConnected] = useState(localStorage.getItem("userId"));

    const Disconnect = () => {
        localStorage.clear();
        window.location.replace("http://localhost:8080/login")
    }
    
    return (
        <div>
            <Navbar collapseOnSelect expand={"md"} bg={"success"} vatiant={"success"}>
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            src={"https://bootstraplogos.com/wp-content/uploads/edd/2017/11/logo-9.png"}
                            height={30}
                            width={40}
                            className={"d-inline-block align-top"}
                            alt={"Logo"}
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls={"responsive-navbar-nav"}/>
                    <Navbar.Collapse id={"responsive-navbar-nav"}>
                        <Nav className={"me-auto mb-2 mb-lg-0"}>
                        <Nav.Link href={"/"} className={"text-light"}>Home Page</Nav.Link>
                        {connected === null ? <></> : <Nav.Link href={"/blog"} className={"text-light"}>Add Post</Nav.Link>}
                        {connected !== null ? <></> : <Nav.Link href={"/register"} className={"text-light"}>Register</Nav.Link>}
                        {connected !== null ? <></> : <Nav.Link href={"/login"} className={"text-light"}>Login</Nav.Link>}
                        {connected === null ? <></> : <Nav.Link href={"/user"} className={"text-light"}>Profile</Nav.Link>}
                        <Nav.Link href={"/about"} className={"text-light"}>About us</Nav.Link>
                        </Nav>
                        <Form inline style={{display: "inline-flex"}}>
                            <FormControl
                                type={"text"}
                                placeholder={"Search"}
                                className={"me-sm-2"}
                            />
                            <Button variant={"outline-info"}>Search</Button>
                            { connected === null ? <></> : <Button variant={"outline-info"} onClick={Disconnect}>Disconnect</Button>}
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Router>
                <Switch>
                <Route path="/login">
                    <Login/>
                </Route>
                <Route path="/" exact>
                    <HomePage/>
                </Route>
                <Route path="/register">
                    <Register/>
                </Route>
                <Route path={"/blog"}>
                    <AddPost/>
                </Route>
                <Route path={"/user"}>
                    <Profile/>
                </Route>
                <Route path={"/about"}>
                    <AboutUs/>
                </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default Header;