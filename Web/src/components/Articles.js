import axios from 'axios';
import React, {useEffect, useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

function Articles() {
    const [content, setContent] = useState(null);

    useEffect(() => {
        axios.get("http://localhost:5000/Article")
            .then((e) => {
                console.log(e);
                setContent(e.data)
        })
            .catch((error) => {
            alert("An error occured")
           console.log(error);
        });
    }, [])

    return (
        <div>
            {content !== null && content.length !== 0 ? (content.map((key) => {
                return (
                    <div className="card" style={{ marginBottom: "10px" }}>
                        <div className="card-header">
                            {key.title}
                            <div style={{ float: "right", display: "inline-flex"}}>
                                <p className="card-text" style={{ paddingRight: "8px", fontSize: "21px"}} >{ key.like_nb}</p>
                                <button type="button" className="btn btn-primary btn-rounded" style={{ "backgroundColor": "green", width: "35px", height: "35px", padding: "0px"}}>
                                    <img src="https://icones.pro/wp-content/uploads/2021/04/icone-noire-noir.png" style={{ width: "30px", height: "30px" }}/>
                                </button>
                            </div>
                        </div>
                            <div className="card-body">
                            <h5 className="card-title">{key.text}</h5>
                            <p className="card-text">{ key.author }</p>
                        </div>
                    </div>
                )
            })): <></>}
        </div>
    );
}

export default Articles;